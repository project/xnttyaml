<?php

namespace Drupal\xnttyaml\Plugin\ExternalEntities\StorageClient;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\external_entities\Plugin\ExternalEntities\StorageClient\FileClientBase;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

/**
 * External entities storage client for Yaml files.
 *
 * @StorageClient(
 *   id = "xnttyaml",
 *   label = @Translation("Yaml Files"),
 *   description = @Translation("Retrieves and stores records in Yaml files.")
 * )
 */
class YamlFiles extends FileClientBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    TranslationInterface $string_translation,
    LoggerChannelFactoryInterface $logger_factory,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    ConfigFactory $config_factory,
    MessengerInterface $messenger,
    CacheBackendInterface $cache
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $string_translation,
      $logger_factory,
      $entity_type_manager,
      $entity_field_manager,
      $config_factory,
      $messenger,
      $cache
    );
    // Defaults.
    $this->fileType = 'yaml';
    $this->fileTypePlural = 'Yaml';
    $this->fileTypeCap = 'Yaml';
    $this->fileTypeCapPlural = 'Yaml';
    $this->fileExtensions = ['.yml', '.yaml'];
  }

  /**
   * {@inheritdoc}
   */
  protected function parseFile(string $file_path) :array {
    $data = [];
    $idf = $this->getSourceIdFieldName() ?? 0;

    // Extract field values from file path.
    $path_data = [];
    $structure = $this->configuration['structure'];
    if (FALSE !== strpos($structure, '{')) {
      $path_data = $entity = $this->getValuesFromPath($file_path);
    }

    try {
      $yaml = Yaml::parseFile($file_path);
      if ($this->configuration['multi_records']) {
        $columns = [];
        // Multiple records, we assume we have an array.
        if (is_array($yaml)) {
          // Process each entity.
          foreach ($yaml as $entity) {
            if (!empty($entity[$idf])) {
              $data[$entity[$idf]] = $entity + $path_data;
            }
            $columns += array_flip(array_keys($entity));
          }
        }
        $data[''] = array_keys($columns);
      }
      else {
        // Single record. We assume we have one entity by file.
        if (is_array($yaml)) {
          // Set a default id value for invalid mappings.
          $yaml[$idf] ??= current($yaml);
          $data[$yaml[$idf]] = $yaml + $path_data;
          $data[''] = array_keys($yaml);
        }
      }
    }
    catch (ParseException $e) {
      $err_line = $e->getParsedLine();
      if (0 <= $err_line) {
        $this->logger->error(
          'Failed to parse Yaml file "'
          . $file_path
          . '" near line '
          . $e->getParsedLine()
          . ': '
          . $e->getSnippet()
        );
      }
      else {
        $this->logger->error(
          'Failed to parse Yaml file "'
          . $file_path
          . '"'
        );
      }
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  protected function generateRawData(array $entities_data) :string {
    // Don't save columns.
    $columns = array_flip($entities_data['']);
    unset($entities_data['']);
    if ($this->configuration['multi_records']) {
      // Multiple values.
      $yaml_entities = [];
      foreach ($entities_data as $id => $entity) {
        $yaml_entities[] = array_intersect_key($entity, $columns);
      }
      $yaml_raw = Yaml::dump($yaml_entities);
    }
    else {
      // Single value.
      $entity = array_shift($entities_data);
      $yaml_entity = array_intersect_key($entity, $columns);
      $yaml_raw = Yaml::dump($yaml_entity);
    }
    // Write records.
    return $yaml_raw;
  }

}
